<?php
/*
Template Name: Catalog Page
*/
get_header(); ?>
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				<h1 class="inside-title">Catálogo de Productos</h1>
			</article>
		</div>
		<div class="row">
			<article class="col-md-3">
				<?php get_template_part('catalog-cat'); ?>
			</article>
			<article class="col-md-9">
				<div class="row">
						<?php
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
							$args = array( 'post_type' => 'catalogo', 'posts_per_page'=>9,'paged' => $paged, 'orderby'=>'title','order'=>'ASC');
						    $loop = new WP_Query( $args );
						    if ( $loop->have_posts() ) :
						        while ( $loop->have_posts() ) : $loop->the_post(); ?>
						    		<?php $link = get_post_meta( $post->ID, '_productotipo_text', true); ?> 
						        	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
						            <article class="col-md-4">
						            	<div class="pindex">
							                <?php if ( has_post_thumbnail() ) { ?>
							                <a href="<?php the_permalink(); ?>">
							                    <div class="product-img-outter">
							                    	<div class="catalogue-img" style="background-image: url('<?php echo $thumb['0'];?>');">
							                    	</div>
							                    	<div class="overlay2"></div>
							                    </div>
							                </a>
							                <?php } else{ ?>
												<a href="<?php the_permalink(); ?>">
							                		<div class="product-img-outter">
							                			<div class="catalogue-img-default"></div>	
							                			<div class="overlay2"></div>
							                		</div>
							                	</a>					                	 
							                <?php } ?>
							                <div class="pindex-inner">
							                    <h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></h3></a>
							                    <hr>
							                    <p>Tipo: <?php echo $link; ?> </p>
							                </div>
							            </div>
						            </article>
						        <?php endwhile; endif;
						    wp_reset_postdata();
						?>
				</div>
			</article>
		</div>
		<div class="row">
			<article class="col-md-12 text-center">
				<ul class="list-inline">
					<li>
						<div class="navigation"><?php previous_posts_link( '<i class="fa fa-angle-left" aria-hidden="true"></i>' ); ?></div>
					</li>
					<li>
						<div class="navigation"><?php next_posts_link( '<i class="fa fa-angle-right" aria-hidden="true"></i>', $loop->max_num_pages ); ?></div>
					</li>
					
				</ul>
			</article>
		</div>
	</section>
<?php get_footer(); ?>