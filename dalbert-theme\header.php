<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title><?php bloginfo ("name"); ?></title>
	<?php wp_head(); ?>
</head>
<body>
	<div class="container-fluid heading" id="top">
		<div class="container">
			<div class="row">
				<article class="col-md-6">
					<p>
						<span><i class="fa <?php echo get_theme_mod('header_section_edit_icon_1') ?>"></i> <?php echo get_theme_mod('header_section_edit_1'); ?></span>
						<span><i class="fa <?php echo get_theme_mod('header_section_edit_icon_2') ?>"></i>  <?php echo get_theme_mod('header_section_edit_2'); ?></span>
					</p>
				</article>
				<article class="col-md-6 text-right">
					<ul class="list-inline">
						<li><a target="_blank" href="<?php echo get_theme_mod('header_section_edit_icon_3_url') ?>"><i class="fa <?php echo get_theme_mod('header_section_edit_icon_3') ?>" aria-hidden="true"></i></a></li>
						<li><a target="_blank" href="#"><i class="fa <?php echo get_theme_mod('header_section_edit_icon_4') ?>" aria-hidden="true"></i></a></li>
						<li><a target="_blank" href="#"><i class="fa <?php echo get_theme_mod('header_section_edit_icon_5') ?>" aria-hidden="true"></i></a></li>
						<li><a target="_blank" href="#"><i class="fa <?php echo get_theme_mod('header_section_edit_icon_6') ?>" aria-hidden="true"></i></a></li>
					</ul>
				</article>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-default shadow1" id="header">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?php echo home_url(); ?>">
	          <img src="<?php echo get_theme_mod('logo'); ?> " alt="logo" class="logo" />
	      </a>
	    </div>

	        <?php
	        	if (is_front_page()) {
	            wp_nav_menu( array(
	                'menu'              => 'primary',
	                'theme_location'    => 'primary',
	                'depth'             => 2,
	                'container'         => 'div',
	                'container_class'   => 'collapse navbar-collapse',
	        		'container_id'      => 'bs-example-navbar-collapse-1',
	                'menu_class'        => 'nav navbar-nav navbar-right',
	                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	                'walker'            => new wp_bootstrap_navwalker())
	            );
	             ?>
	    		<?php } else { ?>
	    		<?php
	            wp_nav_menu( array(
	                'menu'              => 'secondary',
	                'theme_location'    => 'secondary',
	                'depth'             => 2,
	                'container'         => 'div',
	                'container_class'   => 'collapse navbar-collapse',
	        		'container_id'      => 'bs-example-navbar-collapse-1',
	                'menu_class'        => 'nav navbar-nav navbar-right',
	                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	                'walker'            => new wp_bootstrap_navwalker())
	            );?>
	    		<?php } ?>
	    </div>
	</nav>
