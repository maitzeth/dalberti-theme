<?php
	add_action('customize_register', 'customize_register');
	add_action('wp_head', 'customize_css');
	function customize_register($wp_customize){
		// ========================================  PANEL OPCIONES GENERALES ==============================================
	$wp_customize->add_panel('layout', array(
	'priority' => 10,
		'capability' => 'edit_theme_options',
		'title' => __( 'Editar Layout Principal', 'dalbert' ),
		'description' => __( 'Edit All Layouts from Theme', 'dalbert' )
	));

	

	$wp_customize->add_section( 'header_section' , array(
		'title'       => __( 'Header Section', 'dalbert' ),
		'description' => 'Modificar Header Section',
		'priority'    => 10,
		'panel' => 'layout'
	));

	$wp_customize->add_section( 'navbar_section' , array(
		'title'       => __( 'Navbar Section', 'dalbert' ),
		'description' => 'Modificar Elementos del Navbar',
		'priority'    => 11,
		'panel' => 'layout'
	));


	$wp_customize->add_setting( 'logo', array(
		'default' => get_template_directory_uri().'/assets/img/logo.png'
	));

	$wp_customize->add_control(new WP_Customize_Image_Control(
	    $wp_customize,
	    'logo',
	    array(
	      'label'      => __( 'Cambiar Logo', 'dalbert' ),
	      'section'    => 'navbar_section',
	      'settings'   => 'logo',
	      'context'    => 'your_setting_context'
	    )
	));




	$wp_customize->add_setting( 'header_section_edit_color', array(
		'default' => '#4285F4',
		'transport'   => 'refresh'
	));
	
	$wp_customize->add_setting( 'header_section_edit_icon_1', array(
		'default' => 'fa-phone'
	));

	$wp_customize->add_setting( 'header_section_edit_1', array(
		'default' => '+58 666 999 6666'
	));

	$wp_customize->add_setting( 'header_section_edit_icon_2', array(
		'default' => 'fa-map-marker'
	));

	$wp_customize->add_setting( 'header_section_edit_2', array(
		'default' => 'Caracas, Venezuela'
	));

	$wp_customize->add_setting( 'header_section_edit_icon_3_url', array(
		'default' => 'http://www.google.com'
	));

	$wp_customize->add_setting( 'header_section_edit_icon_3', array(
		'default' => 'fa-facebook-square'
	));

	$wp_customize->add_setting( 'header_section_edit_icon_4_url', array(
		'default' => 'http://www.google.com'
	));

	$wp_customize->add_setting( 'header_section_edit_icon_4', array(
		'default' => 'fa-twitter'
	));
	$wp_customize->add_setting( 'header_section_edit_icon_5_url', array(
		'default' => 'http://www.google.com'
	));

	$wp_customize->add_setting( 'header_section_edit_icon_5', array(
		'default' => 'fa-youtube-play'
	));

	$wp_customize->add_setting( 'header_section_edit_icon_6_url', array(
		'default' => 'http://www.google.com'
	));

	$wp_customize->add_setting( 'header_section_edit_icon_6', array(
		'default' => 'fa-google'
	));


	$wp_customize->add_control(new WP_Customize_Color_Control(
	    $wp_customize,
	    'header_section_edit_color',
	    array(
	      'label'      => __( 'Cambio de color header', 'dalbert' ),
	      'section'    => 'header_section',
	      'settings'   => 'header_section_edit_color',
	      'context'    => 'your_setting_context',
	    )
  	));


	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'header_section_edit_icon_1',
		array(
		'label'      => __( 'Primer Icono', 'dalbert' ),
		'section'    => 'header_section',
		'settings'   => 'header_section_edit_icon_1',
		'type'    => 'select',
		'choices'        => array(
                'fa-phone'   => __( 'fa-phone' ),
                'light'  => __( 'Light' )
            )
		)
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'header_section_edit_icon_2',
		array(
		'label'      => __( 'Segundo Icono', 'dalbert' ),
		'section'    => 'header_section',
		'settings'   => 'header_section_edit_icon_2',
		'type'    => 'select',
		'choices'        => array(
                'fa-map-marker'   => __( 'fa-map-marker' ),
                'light'  => __( 'Light' )
            )
		)
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'header_section_edit_1',
		array(
		'label'      => __( 'Primer Campo', 'dalbert' ),
		'section'    => 'header_section',
		'settings'   => 'header_section_edit_1',
		'context'    => 'text'
		)
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'header_section_edit_2',
		array(
		'label'      => __( 'Segundo Campo', 'dalbert' ),
		'section'    => 'header_section',
		'settings'   => 'header_section_edit_2',
		'context'    => 'text'
		)
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'header_section_edit_icon_3',
		array(
		'label'      => __( 'Tercer Icono', 'dalbert' ),
		'section'    => 'header_section',
		'settings'   => 'header_section_edit_icon_3',
		'type'    => 'select',
		'choices'        => array(
                'fa-phone'   => __( 'fa-phone' ),
                'light'  => __( 'Light' )
            )
		)
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'header_section_edit_icon_3_url',
		array(
		'label'      => __( 'URL Tercer Icono', 'dalbert' ),
		'section'    => 'header_section',
		'settings'   => 'header_section_edit_icon_3_url',
		'context'    => 'text'
		)
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'header_section_edit_icon_4',
		array(
		'label'      => __( 'Cuarto Icono', 'dalbert' ),
		'section'    => 'header_section',
		'settings'   => 'header_section_edit_icon_4',
		'type'    => 'select',
		'choices'        => array(
                'fa-phone'   => __( 'fa-phone' ),
                'light'  => __( 'Light' )
            )
		)
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'header_section_edit_icon_5',
		array(
		'label'      => __( 'Quinto Icono', 'dalbert' ),
		'section'    => 'header_section',
		'settings'   => 'header_section_edit_icon_5',
		'type'    => 'select',
		'choices'        => array(
                'fa-phone'   => __( 'fa-phone' ),
                'light'  => __( 'Light' )
            )
		)
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'header_section_edit_icon_6',
		array(
		'label'      => __( 'Sexto Icono', 'dalbert' ),
		'section'    => 'header_section',
		'settings'   => 'header_section_edit_icon_6',
		'type'    => 'select',
		'choices'        => array(
                'fa-phone'   => __( 'fa-phone' ),
                'light'  => __( 'Light' )
            )
		)
	));

	// ======================================================
	// ================== HEADER SECTION ====================
	// ======================================================

	$wp_customize->add_section( 'header_image_section' , array(
		'title'       => __( 'Header Image Section', 'dalbert' ),
		'description' => 'Modificar Elementos del Header',
		'priority'    => 11,
		'panel' => 'layout'
	));

	$wp_customize->add_setting( 'header_image', array(
		'default' => get_template_directory_uri().'/assets/img/bg.jpg'
	));

	$wp_customize->add_control(new WP_Customize_Image_Control(
	    $wp_customize,
	    'header_image',
	    array(
	      'label'      => __( 'Cambiar Imagen de Cabecera', 'dalbert' ),
	      'section'    => 'header_image_section',
	      'settings'   => 'header_image',
	      'context'    => 'your_setting_context'
	    )
	));

	$wp_customize->add_setting( 'heading_title', array(
		'default' => 'SOMOS GENTE DALBERT, HAGA NEGOCIOS CON NOSOTROS'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'heading_title',
		array(
		'label'      => __( 'Titulo de la cabecera', 'dalbert' ),
		'section'    => 'header_image_section',
		'settings'   => 'heading_title',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'btn_title', array(
		'default' => 'Saber mas'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'btn_title',
		array(
		'label'      => __( 'Titulo del boton', 'dalbert' ),
		'section'    => 'header_image_section',
		'settings'   => 'btn_title',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'btn_url', array(
		'default' => '#'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'btn_url',
		array(
		'label'      => __( 'Url del Boton', 'dalbert' ),
		'section'    => 'header_image_section',
		'settings'   => 'btn_url',
		'context'    => 'text'
		)
	));

	// =======================================
	// ================= ABOUT ===============
	// =======================================

	$wp_customize->add_section( 'about_section' , array(
		'title'       => __( 'About Section', 'dalbert' ),
		'description' => 'Modificar Elementos del la seccion de NOSOTROS',
		'priority'    => 12,
		'panel' => 'layout'
	));

	$wp_customize->add_setting( 'about_title', array(
		'default' => 'QUIENES SOMOS'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'about_title',
		array(
		'label'      => __( 'Titulo NOSOTROS', 'dalbert' ),
		'section'    => 'about_section',
		'settings'   => 'about_title',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'about_text', array(
		'default' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam pariatur porro libero, architecto amet doloremque cupiditate ipsam, dicta quasi necessitatibus, voluptatum eaque modi perspiciatis vitae corporis, saepe incidunt consequatur ipsum.'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'about_text',
		array(
		'label'      => __( 'Texto de Nosotros', 'dalbert' ),
		'section'    => 'about_section',
		'settings'   => 'about_text',
		'type'		 => 'textarea',
		'context'    => 'text'
		)
	));


	$wp_customize->add_setting( 'about_img1', array(
		'default' => get_template_directory_uri().'/assets/img/bg.jpg'
	));

	$wp_customize->add_control(new WP_Customize_Image_Control(
	    $wp_customize,
	    'about_img1',
	    array(
	      'label'      => __( 'Cambiar Imagen de NOSOTROS', 'dalbert' ),
	      'section'    => 'about_section',
	      'settings'   => 'about_img1',
	      'context'    => 'your_setting_context'
	    )
	));

	// ===========================================
	// =============== SERVICES ==================
	// ===========================================

	$wp_customize->add_section( 'services_section' , array(
		'title'       => __( 'Services Section', 'dalbert' ),
		'description' => 'Modificar Elementos del la seccion de SERVICIOS',
		'priority'    => 13,
		'panel' => 'layout'
	));

	$wp_customize->add_setting( 'services_title', array(
		'default' => 'SERVICIOS'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'services_title',
		array(
		'label'      => __( 'Titulo SERVICIOS', 'dalbert' ),
		'section'    => 'services_section',
		'settings'   => 'services_title',
		'context'    => 'text'
		)
	));

	// ===========================================
	// ============== PRODUCTOS ==================
	// ===========================================

	$wp_customize->add_section( 'products_section' , array(
		'title'       => __( 'Products Section', 'dalbert' ),
		'description' => 'Modificar Elementos del la seccion de PRODUCTOS',
		'priority'    => 14,
		'panel' => 'layout'
	));

	$wp_customize->add_setting( 'products_text', array(
		'default' => 'Conoce toda la gama de productos que tenemos para tí'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'products_text',
		array(
		'label'      => __( 'Texto del Area', 'dalbert' ),
		'section'    => 'products_section',
		'settings'   => 'products_text',
		'context'    => 'text'
		)
	));


	$wp_customize->add_setting( 'products_url', array(
		'default' => 'http://google.com'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'products_url',
		array(
		'label'      => __( 'URL del Boton', 'dalbert' ),
		'section'    => 'products_section',
		'settings'   => 'products_url',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'products_btn', array(
		'default' => 'Saber más'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'products_btn',
		array(
		'label'      => __( 'TEXTO del Boton', 'dalbert' ),
		'section'    => 'products_section',
		'settings'   => 'products_btn',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'products_bg', array(
		'default' => get_template_directory_uri().'/assets/img/bg1.jpg'
	));

	$wp_customize->add_control(new WP_Customize_Image_Control(
	    $wp_customize,
	    'products_bg',
	    array(
	      'label'      => __( 'Cambiar Imagen de Productos', 'dalbert' ),
	      'section'    => 'products_section',
	      'settings'   => 'products_bg',
	      'context'    => 'your_setting_context'
	    )
	));


	// ===========================================
	// ============== PRODUCTOS ==================
	// ===========================================

	$wp_customize->add_section( 'blog_section' , array(
		'title'       => __( 'Blog Section', 'dalbert' ),
		'description' => 'Modificar Elementos del la seccion de BLOG',
		'priority'    => 15,
		'panel' => 'layout'
	));

	$wp_customize->add_setting( 'blog_title', array(
		'default' => 'EVENTOS Y NOTICIAS'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'blog_title',
		array(
		'label'      => __( 'Titulo del Area del Blog', 'dalbert' ),
		'section'    => 'blog_section',
		'settings'   => 'blog_title',
		'context'    => 'text'
		)
	));

	// ===========================================
	// ============== CONTACTO ==================
	// ===========================================

	$wp_customize->add_section( 'contact_section' , array(
		'title'       => __( 'Contact Section', 'dalbert' ),
		'description' => 'Modificar Elementos del la seccion de BLOG',
		'priority'    => 16,
		'panel' => 'layout'
	));

	$wp_customize->add_setting( 'contact_image', array(
		'default' => get_template_directory_uri().'/assets/img/contact.jpg'
	));

	$wp_customize->add_control(new WP_Customize_Image_Control(
	    $wp_customize,
	    'contact_image',
	    array(
	      'label'      => __( 'Cambiar imagen fondo Contacto', 'dalbert' ),
	      'section'    => 'contact_section',
	      'settings'   => 'contact_image',
	      'context'    => 'your_setting_context'
	    )
	));

	$wp_customize->add_setting( 'contact_title', array(
		'default' => 'Contactanos'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_title',
		array(
		'label'      => __( 'Titulo del Area Contactanos', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_title',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'keep_touch', array(
		'default' => 'Mantente en contacto'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'keep_touch',
		array(
		'label'      => __( 'Subtitulo del Area Contactanos', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'keep_touch',
		'context'    => 'text'
		)
	));


	$wp_customize->add_setting( 'text_contact', array(
		'default' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi, veritatis ex cum voluptatibus ipsam officiis nobis, alias cumque vitae aliquid quas odit, sunt, illo dolore. Impedit quia ad accusamus cupiditate!'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'text_contact',
		array(
		'label'      => __( 'Texto del area de Contacto', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'text_contact',
		'type'       => 'textarea'
		)
	));

	$wp_customize->add_setting( 'contact_logo_1', array(
		'default' => 'fa-facebook'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_1',
		array(
		'label'      => __( 'Primer Logo', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_1',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'contact_logo_1_url', array(
		'default' => 'http://google.com'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_1_url',
		array(
		'label'      => __( 'Primer Logo URL', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_1_url',
		'context'    => 'text'
		)
	));



	$wp_customize->add_setting( 'contact_logo_2', array(
		'default' => 'fa-twitter'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_2',
		array(
		'label'      => __( 'Segundo Logo', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_2',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'contact_logo_2_url', array(
		'default' => 'http://google.com'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_2_url',
		array(
		'label'      => __( 'Segundo Logo URL', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_2_url',
		'context'    => 'text'
		)
	));




	$wp_customize->add_setting( 'contact_logo_3', array(
		'default' => 'fa-instagram'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_3',
		array(
		'label'      => __( 'Tercer Logo', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_3',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'contact_logo_3_url', array(
		'default' => 'http://google.com'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_3_url',
		array(
		'label'      => __( 'Tercer Logo URL', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_3_url',
		'context'    => 'text'
		)
	));



	$wp_customize->add_setting( 'contact_logo_4', array(
		'default' => 'fa-envelope-o'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_4',
		array(
		'label'      => __( 'Tercer Logo', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_4',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'contact_logo_4_url', array(
		'default' => 'http://google.com'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_4_url',
		array(
		'label'      => __( 'Cuarto Logo URL', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_4_url',
		'context'    => 'text'
		)
	));






	$wp_customize->add_setting( 'contact_logo_5', array(
		'default' => 'fa-map-marker'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_5',
		array(
		'label'      => __( 'Quinto Logo', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_5',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'contact_logo_5_text', array(
		'default' => 'Caracas, Venezuela'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_5_text',
		array(
		'label'      => __( 'Texto Quinto Logo', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_5_text',
		'context'    => 'text'
		)
	));




	$wp_customize->add_setting( 'contact_logo_6', array(
		'default' => 'fa-phone'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_6',
		array(
		'label'      => __( 'Sexto Logo', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_6',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'contact_logo_6_text', array(
		'default' => '+56 666 66 66'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_6_text',
		array(
		'label'      => __( 'Texto Sexto Logo', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_6_text',
		'context'    => 'text'
		)
	));




	$wp_customize->add_setting( 'contact_logo_7', array(
		'default' => 'fa-envelope-o'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_7',
		array(
		'label'      => __( 'Septimo Logo', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_7',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'contact_logo_7_text', array(
		'default' => 'email@email.com'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'contact_logo_7_text',
		array(
		'label'      => __( 'Texto Septimo Logo', 'dalbert' ),
		'section'    => 'contact_section',
		'settings'   => 'contact_logo_7_text',
		'context'    => 'text'
		)
	));

	/* =============================== */
	/* ========= BLOG HEADER ========= */
	/* =============================== */

	$wp_customize->add_panel('blog', array(
	'priority' => 11,
		'capability' => 'edit_theme_options',
		'title' => __( 'Editar Blog Layout', 'dalbert' ),
		'description' => __( 'Edit All Layouts from Theme', 'dalbert' )
	));

	$wp_customize->add_section( 'blog_header_section' , array(
		'title'       => __( 'Blog Header', 'dalbert' ),
		'description' => 'Modificar Header Section',
		'priority'    => 10,
		'panel' => 'blog'
	));


	$wp_customize->add_setting( 'blog_header_title', array(
		'default' => 'Te invitamos a leer nuestras noticias'
	));

	$wp_customize->add_control(new WP_Customize_Control(
		$wp_customize,
		'blog_header_title',
		array(
		'label'      => __( 'Titulo del Blog Layout', 'dalbert' ),
		'section'    => 'blog_header_section',
		'settings'   => 'blog_header_title',
		'context'    => 'text'
		)
	));

	$wp_customize->add_setting( 'blog_header_img', array(
		'default' => get_template_directory_uri().'/assets/img/blog-bg.jpg'
	));

	$wp_customize->add_control(new WP_Customize_Image_Control(
	    $wp_customize,
	    'blog_header_img',
	    array(
	      'label'      => __( 'Cambiar Imagen de Cabecera Blog', 'dalbert' ),
	      'section'    => 'blog_header_section',
	      'settings'   => 'blog_header_img',
	      'context'    => 'your_setting_context'
	    )
	));

	
	}

	function customize_css(){
    ?>
      <style type="text/css">
        .heading{
        	background-color: <?php echo get_theme_mod('header_section_edit_color');?>
        }

        header{
        	background-image: url(<?php echo get_theme_mod('header_image');?>);
        }

        .product{
			background-image: url(<?php echo get_theme_mod('products_bg'); ?>);
		}

		.contact {
    		background-image: url(<?php echo get_theme_mod('contact_image'); ?>);
    	}
      </style>
    <?php
  }
?>