<?php
/*
Template Name: Front Page
*/
get_header(); ?>
<header class="shadow1 parallax">
	<div class="header-inner">
		<h1 class="text-left">
		<?php echo get_theme_mod('heading_title'); ?>
		</h1>
		<a href="<?php echo get_theme_mod('btn_url'); ?>" class="btn btn-primary btn-lg page-scroll"><?php echo get_theme_mod('btn_title'); ?></a>
	</div>
	<div class="overlay"></div>
</header>
<section class="container" id="about">
	<div class="row">
		<article class="col-md-12">
			<h1 class="title-section"><?php echo get_theme_mod('about_title'); ?></h1>
		</article>
	</div>
	<div class="row">
		<article class="col-md-8">
			<p><?php echo get_theme_mod('about_text'); ?></p>
		</article>
		<article class="col-md-4">
			<img src="<?php echo get_theme_mod('about_img1'); ?>" alt="" class="img-responsive center-block">
		</article>
	</div>
</section>
<section class="container-fluid">
	<section class="container" id="services">
		<div class="row">
			<article class="col-md-12">
				<h1 class="title-section">Servicios</h1>
			</article>
			<div class="row">
				<?php
				$args = array(
				'post_type' => 'services',
				'posts_per_page' => 9,
				'order'          => 'DESC' );
				$loop = new WP_Query($args);
				?>
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<article class="col-md-4 text-center">
					<div class="service-box">
						<i class="fa <?= get_post_meta( $post->ID, 'service-icon', true); ?> services-size"></i>
						<h2><?php the_title(); ?></h2>
						<p class="text-center">
							<?php the_excerpt() ?>
						</p>
						<?php $service = get_post_meta( $post->ID, 'service-url', true) ?>
						<?php if(!empty($service)): ?>
						<a href="<?= get_post_meta( $post->ID, 'service-url', true); ?>" class="btn btn-link service-link">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
						<?php endif; ?>
					</div>
				</article>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
</section>


<section class="product container-fluid parallax" id="products">
	<div class="product-inner">
		<h1><?= get_theme_mod('products_text') ?></h1>
		<a href="<?= get_theme_mod('products_url') ?>" class="btn btn-primary btn-lg"><?= get_theme_mod('products_btn') ?></a>
	</div>
	<div class="overlay"></div>
</section>


<section class="container" id="blog">
	<div class="row">
		<article class="col-md-12">
			<h1 class="title-section"><?= get_theme_mod('blog_title') ?></h1>
		</article>
	</div>
	<div class="post-entry">
				<?php
					global $post;
					$args = array( 
						'posts_per_page' => 3, 
						'offset'         => 0, 
						'post_status'    => 'publish',
						'orderby'        => 'date',
					);
					$myposts = get_posts( $args );
				?>
				<?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
						<div class="row mgt">
							<article class="col-md-4">
								<?php if ( has_post_thumbnail() ) : ?>
								    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								        <div class="post-img shadow1" style="background-image: url('<?php echo $thumb['0'];?>')">
								        	<div class="overlay-circle"></div>
								        </div>
								    </a>
								<?php else : ?>
									<img src="http://placehold.it/220x220" alt="" class="img-responsive img-circle center-block shadow1">
								<?php endif; ?>
							</article>
							<article class="col-md-8">
								<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
								<?php the_excerpt() ?>
								<div class="entry-meta">
									<span class="posted-on">
										<?php echo get_the_date(); ?>
									</span>
									<span class="byline">
										<span class="author vcard"><?php the_author_posts_link() ?></span>
									</span>
									<span class="cat-link"><?php the_category( ', ' ); ?>
									</span>
								</div>
							</article>
						</div>
				<?php endforeach; wp_reset_postdata(); ?>
	</div>
	<div class="row">
		<article class="col-md-12 text-center">
			<hr>
			<br>
			<a href="<?php echo get_permalink( get_page_by_path( 'blog' ) ); ?>" class="mybtn shadow1">Ver todos</a>
		</article>
	</div>
</section>

		<section class="contact container-fluid" id="contact">
			<div class="container" style="position: relative;z-index: 2;">
				<div class="row">
					<article class="col-md-12">
						<h1 class="title-section"><?php echo get_theme_mod('contact_title'); ?></h1>
					</article>
				</div> 
				<div class="row">
					<div class="col-md-4 address">
						<h4><?php echo get_theme_mod('keep_touch'); ?></h4>
						<p class="cnt-p"><?php echo get_theme_mod('text_contact'); ?> </p>
						<div class="social-icons">
							<ul class="list-inline">
								<li><a href="<?php echo get_theme_mod('contact_logo_1_url'); ?>" target="_blank"><i class="fa <?php echo get_theme_mod('contact_logo_1'); ?>" aria-hidden="true"></i></a></li>
								<li><a href="<?php echo get_theme_mod('contact_logo_2_url'); ?>" target="_blank"><i class="fa <?php echo get_theme_mod('contact_logo_2'); ?>" aria-hidden="true"></i></a></li>
								<li><a href="<?php echo get_theme_mod('contact_logo_3_url'); ?>" target="_blank"><i class="fa <?php echo get_theme_mod('contact_logo_3'); ?>" aria-hidden="true"></i></a></li>
								<li><a href="<?php echo get_theme_mod('contact_logo_4_url'); ?>" target="_blank"><i class="fa <?php echo get_theme_mod('contact_logo_4'); ?>" aria-hidden="true"></i></a></li>
							</ul>
						</div>
						<p><i class="fa <?php echo get_theme_mod('contact_logo_5'); ?>" aria-hidden="true"></i> <?php echo get_theme_mod('contact_logo_5_text'); ?> </p>
						<p><i class="fa <?php echo get_theme_mod('contact_logo_6'); ?>" aria-hidden="true"></i> <?php echo get_theme_mod('contact_logo_6_text'); ?></p>
						<p><i class="fa <?php echo get_theme_mod('contact_logo_7'); ?>" aria-hidden="true"></i> <a href="mailto:<?php echo get_theme_mod('contact_logo_7_text'); ?>"><?php echo get_theme_mod('contact_logo_7_text'); ?></a></p>
					</div>
					<div class="col-md-8 contact-form">
						<?php if ( is_active_sidebar( 'contact-widget-area' ) ) : ?>
				            <?php dynamic_sidebar( 'contact-widget-area' ); ?>
				        <?php endif; ?>
						<!-- <form action="#" method="post" _lpchecked="1">
							
							<input type="text" name="Name" placeholder="Nombre" required="">
							<input class="email" type="text" name="Email" placeholder="Email" required="">
							<textarea placeholder="Message" name="Mensaje..." required=""></textarea>
							<input type="submit" value="Enviar">
						</form> -->
					</div>
					<div class="clearfix"></div>	
				</div>
			</div>
			<div class="overlay"></div>
		</section>
<?php get_footer(); ?>