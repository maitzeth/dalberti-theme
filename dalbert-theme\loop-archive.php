<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
    <div class="row">
      <article class="col-sm-12">
        <div class="post-img-2 shadow1" style="background-image: url('<?php echo $thumb['0'];?>');"></div>
        <h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>
        <h5><?php the_date();?> | <?php the_author_posts_link() ?></h5>
        <p>
          <?php the_content(); ?>
        </p>

      </article>
    </div>

<?php endwhile; else: ?>
  <p>
  <?php _e('Lo sentimos no hay contenido relacionado a su busqueda. LOOP ARCHIVE'); ?>
  </p>
<?php endif; ?>
