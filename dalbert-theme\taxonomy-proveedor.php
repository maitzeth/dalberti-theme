<?php get_header(); ?>

<section class="container">
	<div class="row">
		<article class="col-md-12">
			<h1 class="inside-title">Proveedor: <?php single_term_title(); ?> </h1>
		</article>
	</div>
	<div class="row">
		<ul class="breadcrumb">
		    <li><a href="<?php echo get_permalink( get_page_by_path( 'catalogo' ) ); ?>">Ver todos</a></li>
		    <?php
		        $term = get_term_by("slug", get_query_var("term"), get_query_var("taxonomy") );
		        $tax = get_taxonomy( $taxonomy ) ;
		    ?>
			<?php
		        echo '<li>' . $term->name . '</li>';
		    ?>
		</ul>
	</div>
	<div class="row">
		<article class="col-md-3">
			<?php get_template_part('catalog-cat'); ?>
		</article>
		<article class="col-md-9">
			<?php include(locate_template('loop-catalogo.php')); ?>
		</article>
	</div>
</section>

<?php get_footer(); ?>
