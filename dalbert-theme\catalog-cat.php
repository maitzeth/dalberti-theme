<div class="taxonomy">
	<div class="form-group">
		<h2>
		Sector Industrial:
		</h2>
		<?php $terms = get_terms( 'sector' ); ?>
		<div class="dropdown">
			<button class="btn btn-default btn-block custom-class dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			Seleccione
			<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
				<?php
					foreach ( $terms as $term ) {
						$term_link = get_term_link( $term );
						if ( is_wp_error( $term_link ) ) {
							continue;
						}
						echo '<li><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
					}
				?>
			</ul>
		</div>
	</div>
	<div class="form-group">
		<h2>
		Proveedor:
		</h2>
		<?php $theterms = get_terms( 'proveedor' ); ?>
		<div class="dropdown">
			<button class="btn btn-default btn-block custom-class dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			Seleccione
			<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
				<?php
					foreach ( $theterms as $theterm ) {
					$term_link = get_term_link( $theterm );
					echo '<li><a href="' . esc_url( $term_link ) . '">' . $theterm->name . '</a></li>';
					}
				?>
			</ul>
		</div>
	</div>
	<div class="form-group">
		<h2>
		Funcionalidad:
		</h2>
		<?php $terminos = get_terms( 'funcionalidad' ); ?>
		<div class="dropdown">
			<button class="btn btn-default btn-block custom-class dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			Seleccione
			<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
				<?php
					foreach ( $terminos as $termino ) {
					$term_link = get_term_link( $termino );
					echo '<li><a href="' . esc_url( $term_link ) . '">' . $termino->name . '</a></li>';
					}
				?>
			</ul>
		</div>
	</div>
</div>