<?php
	$mostrados = [];
	$taxonomies = get_object_taxonomies( array( 'post_type' => 'catalogo', 'orderby'=>'title', 'order'=>'ASC' ) );
	foreach( $taxonomies as $taxo ):
		if( $taxo != $tax->name ){
			continue;
		}
		$terms = get_terms( $taxo );
			foreach( $terms as $termino ):
				if( $termino->slug != $term->slug ){
					continue;
				}
				$posts = new WP_Query( "taxonomy=$taxo&term=$termino->slug&posts_per_page=150&orderby=title&order=ASC" );
					if( $posts->have_posts() ): while( $posts->have_posts() ): $posts->the_post();
						if ( in_array($post->ID, $mostrados) ){
							continue;
						}

						$mostrados[] = $post->ID;
?>
						<?php $link = get_post_meta( $post->ID, '_productotipo_text', true); ?> 
						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
						<article class="col-md-4">
							<div class="pindex">
								<?php if ( has_post_thumbnail() ) { ?>
									<a href="<?php the_permalink(); ?>">
										<div class="product-img-outter">
											<div class="catalogue-img" style="background-image: url('<?php echo $thumb['0'];?>');">
											</div>
											<div class="overlay2"></div>
										</div>
									</a>
								<?php } else{ ?>
									<a href="<?php the_permalink(); ?>">
										<div class="product-img-outter">
											<div class="catalogue-img-default"></div>
											<div class="overlay2"></div>
										</div>
									</a>
								<?php } ?>
								<div class="pindex-inner">
									<h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
									<hr>
									<p>Tipo: <?php echo $link; ?></p>
								</div>
							</div>
						</article>
<?php
					endwhile; endif;
			endforeach;
	endforeach;
?>