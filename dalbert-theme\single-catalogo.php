<?php get_header(); ?>
<section class="container">
	<div class="row">
		<article class="col-md-12">
			<h1 class="inside-title">Producto: <?php the_title(); ?></h1>
		</article>
	</div>
	<div class="row">
		<ol class="breadcrumb">
		  <li><a href="<?php echo get_permalink( get_page_by_path( 'catalogo' ) ); ?>">Ver todos</a></li>
		  	<?php echo get_the_term_list( $post->ID, array('sector', 'proveedor', 'funcionalidad'), '<li>', ' - ', '</li>' ); ?>
		  <li class="active"><?php the_title(); ?></li>
		</ol>
	</div>
	<div class="row">
		<article class="col-md-3">
			<?php get_template_part('catalog-cat'); ?>
		</article>
		<article class="col-sm-9">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					
					<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
					<?php $link = get_post_meta( $post->ID, '_productotipo_text', true); ?> 
					<?php $empleo = get_post_meta( $post->ID, '_productofuncionalidad_text', true); ?>

					<?php 
						$taxo_sector = get_the_terms($post->ID , 'sector');
						$sectores = [];
 						if(!empty($taxo_sector)) { 
 							$argsS = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
 							$terminos = wp_get_post_terms( $post->ID, 'sector', $argsS);
 							if (!empty($terminos)) {
 								foreach ( $terminos as $termino ) {
									$term_link = get_term_link( $termino );
									if ( is_wp_error( $term_link ) ) {
										continue;
									}
									$sectores[] = '<li class="list-group-item"><a href="' . esc_url( $term_link ) . '">' . $termino->name . '</a></li>';
								}
 							} 
 						} else {
 							$vacio = '<p>Campo vacío</p>';
 						}



						// =========================================================
						
 						$taxo_proveedor = get_the_terms( $post->ID , 'proveedor');
						$proveedores = [];
 						if(!empty($taxo_proveedor)) { 
 							$argsP = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
 							$terms = wp_get_post_terms( $post->ID, 'proveedor', $argsP);
 							if(!empty($terms)){
 								foreach ($terms as $term){
 									$proveedor_link = get_term_link( $term );
 									if ( is_wp_error( $proveedor_link ) ){
 										continue;
 									}
 									$proveedores[] = '<li class="list-group-item"><a href="' . esc_url( $proveedor_link ) . '">' . $term->name . '</a></li>';
 								}
 							}
 						} else {
 							$vacio = '<p>Campo vacío</p>';
 						}

 						$taxo_funcionalidad = get_the_terms($post->ID, 'funcionalidad');
 						$funcionalidad = [];
 						if (!empty($taxo_funcionalidad)) {
 							$argsF = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
 							$metas = wp_get_post_terms( $post->ID, 'funcionalidad', $argsF);
 							if(!empty($metas)){
 								foreach ($metas as $meta){
 									if( is_wp_error($meta_link) ){
 										continue;
 									}
 									$funcionalidad[] = '<li class="list-group-item"><a href="' . esc_url( $meta_link ) . '">' . $meta->name . '</a></li>';
 								}
 							}
 						} else {
 							$vacio = "Esta vacio";
 						}
 						




					?> 

				    <div class="row">
				      <article class="col-sm-12">
						<div class="catalogue-img2" style="background-image: url('<?php echo $thumb['0'];?>');">
						</div>
				        <div class="panel panel-default">
				        	<div class="panel-body shadow1">
				        		<h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>
						        <p>
						          <?php the_content(); ?>
						        </p>
						        <hr>
						        <p>
							      <h3> Sector Industrial:</h3> 	<?php if (!empty($taxo_sector)): ?> 
							       							<ul class="list-group">
							       								<?php 
							       									foreach($sectores as $sector) {
																	    echo $sector;
																	}
							       								?> 
							       							</ul>
							       						<?php else: ?> 
							       							<?php echo $vacio; ?>
							       						<?php endif ?>
							    </p>
							    <p>
							    	<h3>Proveedor:</h3>			<?php if(!empty($taxo_proveedor)): ?>
							    							<ul class="list-group">
							    						<?php 
							    							foreach($proveedores as $proveedor) {
							    								echo $proveedor;
							    							}
							    						?> 
							    							</ul>
							    					<?php else: ?>
							    							<?php echo $vacio; ?>
							    					<?php endif ?>
							    </p>
						        <p>
						        	<h3>Funcionalidad:</h3><?php if(!empty($taxo_funcionalidad)): ?>
						        							<ul class="list-group">
						        						<?php
						        							foreach($funcionalidad as $funcion){
						        								echo $funcion;
						        							}
						        						?>
						        							</ul>
						        					<?php else: ?>
							    							<?php echo $vacio; ?>
							    					<?php endif ?>

						        </p>


						        <hr>

						        <p>
						        	<b>Tipo:</b> <?php echo $link; ?>
						        </p>
						        <p>
						        	<b>Modo de Empleo:</b> <?php echo $empleo; ?>
						        </p>
				        	</div>
				        </div>
				      </article>
				    </div>
	
				<?php endwhile; else: ?>
				  <p>
				  <?php _e('Lo sentimos no hay contenido relacionado a su busqueda. LOOP ARCHIVE'); ?>
				  </p>
				<?php endif; ?>
		</article>
	</div>
</section>
<?php get_footer(); ?>
