<?php
/**
 * default search form
 */
?>
<form role="search" method="get" id="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="search-wrap">
        <label>
        	<input type="search" class="search-field" placeholder="<?php echo esc_attr( 'Buscar…', 'presentation' ); ?>" name="s" id="search-input" value="<?php echo esc_attr( get_search_query() ); ?>" />
        </label>
        <input class="screen-reader-text btn btn-link btn-lg search-submit" type="submit" id="search-submit" value="Buscar" />
    </div>
</form>