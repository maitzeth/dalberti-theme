<ul class="list-unstyled">
    <?php
        // Register Custom Comment Walker
        require_once('inc/class-wp-bootstrap-comment-walker.php');

        wp_list_comments( array(
            'style'         => 'ul',
            'short_ping'    => true,
            'avatar_size'   => '64',
            'walker'        => new Bootstrap_Comment_Walker(),
        ) );
    ?>
</ul><!-- .comment-list -->