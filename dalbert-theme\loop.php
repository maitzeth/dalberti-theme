<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
  <div class="row">
    <article class="col-sm-12">
      <div class="the-post shadow1">
          <?php if ( has_post_thumbnail() ) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
              <div class="post-img2 shadow1" style="background-image: url('<?php echo $thumb['0'];?>')">
                <div class="overlay-circle2"></div>
              </div>
            </a>
          <?php else : ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <div class="post-img2 shadow1">
                    <div class="overlay-circle2"></div>
                </div>
            </a>
          <?php endif; ?>
          <h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>
          <?php the_excerpt() ?>
            <p>
                <i class="fa fa-user-circle-o" aria-hidden="true"></i> <?php the_author_posts_link() ?>
            </p>
            <p>
                <i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date();?>
            </p>
            <p>
                <i class="fa fa-comments" aria-hidden="true"></i> <?php comments_number( 'no responses', 'one response', '% responses' ); ?>
            </p>
      </div>
    </article>
  </div>
<?php endwhile; else: ?>
<p>
  <?php _e('Lo sentimos no hay contenido relacionado a su busqueda.'); ?>
</p>
<?php endif; ?>