<?php get_header(); ?>
<section class="container">
	<div class="row">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
		<div class="row">
			<article class="col-md-12">
				<div class="post-img-2 shadow1" style="background-image: url('<?php echo $thumb['0'];?>');"></div>
			</article>
		</div>
		<br><br>
		<div class="col-sm-8" style="padding: 0 25px">
			<div class="row">
				    <div class="row">
				      <article class="col-md-12">
				        
				        <h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>
				        <h5><?php the_date();?> | <?php the_author_posts_link() ?></h5>
				        <p>
				          <?php the_content(); ?>
				        </p>

				      </article>
				    </div>
				    <div class="row">
				    	<article class="col-md-12">
				    		<ol class="commentlist">
								<?php
									//Gather comments for a specific page/post 
									$comments = get_comments(array(
										'post_id' => $post->ID,
										'status' => 'approve' //Change this to the type of comments to be displayed
									));

									//Display the list of comments
									wp_list_comments(array(
										'per_page' => 10, //Allow comment pagination
										'reverse_top_level' => false //Show the oldest comments at the top of the list
									), $comments);
								?>
							</ol>
				    	</article>
				    </div>
				    <div class="row">
				    	<article class="col-md-12">
				    		<?php 
				    			$comments_args = array(
								        // Change the title of send button 
								        'label_submit' => __( 'Enviar', 'dalbert' ),
								        // Change the title of the reply section
								        'title_reply' => __( 'Escribe una respuesta o un comentario', 'dalbert' ),
								        // Remove "Text or HTML to be displayed after the set of comment fields".
								        'comment_notes_after' => '',
								        // Redefine your own textarea (the comment body).
								        'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><br /><textarea id="comment" name="comment" aria-required="true" class="form-control" rows="10"></textarea></p>',
								);
								comment_form( $comments_args );
				    		?>
				    	</article>
				    </div>
				<?php endwhile; else: ?>
				  <p>
				  <?php _e('Lo sentimos no hay contenido relacionado a su busqueda.'); ?>
				  </p>
				<?php endif; ?>
			</div>
		</div>
		<div class="col-sm-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
