jQuery(document).ready(function(){
    jQuery(window).scroll(function(){
        var scroll = jQuery(window).scrollTop();
        var position =  (scroll * 0.10);
        jQuery('.parallax').css({
            'background-position': '0 -' + position + 'px'
        });
    });
});