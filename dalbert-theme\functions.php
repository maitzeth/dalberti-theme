<?php
	// Hooks
	add_action('after_setup_theme', 'theme_setup');
	add_action("wp_enqueue_scripts", "add_js", 11);
	add_action('wp_enqueue_scripts', 'add_css');
	add_action('widgets_init','crear_widget_area');
	add_action( 'init', 'create_post_type' );
	add_action( 'init', 'taxonomias_propias' );




	if ( !function_exists( 'theme_setup' ) ):
	function theme_setup() {
		add_editor_style();
		add_theme_support( 'post-thumbnails' );
		add_image_size('max-thumbnail', 640, 250, true);
		add_theme_support('html5', array( 'comment-list' ));
		add_theme_support('menus');
		register_nav_menus( array(
			'primary' => __( 'Primary Navigation', 'dalbert' ),
			'secondary' => __('Secondary Navigation', 'dalbert' )
		));
	}
	endif;

	// Adding JS
	if (!function_exists('add_css')) {
			function add_js(){
	// nos aseguramos que no estamos en el area de administracion
	if( !is_admin() ){
	wp_register_script('bootstrap', get_bloginfo('template_directory'). '/assets/js/bootstrap.min.js', array('jquery'), '1', true );
	wp_register_script('jquery-easing', get_bloginfo('template_directory'). '/assets/js/jquery.easing.1.3.js', array('jquery'), '1', true );
	wp_register_script('jquery-sticky', get_bloginfo('template_directory'). '/assets/js/jquery.sticky.js', array('jquery'), '1', true);
	wp_register_script('scrollto', get_bloginfo('template_directory'). '/assets/js/myscrollto.js', array('jquery-easing'), '1', true );
	wp_register_script('parallax', get_bloginfo('template_directory'). '/assets/js/parallax.js', array('jquery'), '1', true );
	wp_enqueue_script('bootstrap');
	wp_enqueue_script('jquery-easing');
	wp_enqueue_script('jquery-sticky');
	wp_enqueue_script('scrollto');
	}
		}
	}
	if (!function_exists('add_css')) {
	function add_css(){
	wp_register_style ('bootstrap', get_template_directory_uri()."/assets/css/bootstrap.min.css");
	wp_register_style ('fontawesome', get_template_directory_uri()."/assets/css/font-awesome.min.css");
	wp_register_style ('dalbert', get_template_directory_uri()."/style.css", array('bootstrap'));
	wp_enqueue_style("dalbert");
	wp_enqueue_style("fontawesome");
	}
	}
	// Register Custom Navigation Walker
	require_once ('inc/wp_bootstrap_navwalker.php');
	// REGISTRA WIDGETS AREA
	if(!function_exists('crear_widget_area')):
	function crear_widget_area(){
			register_sidebar(array(
			'name' => __('Sidebar Widget Area', 'dalbert'),
			'id' => 'sidebar-widget-area',
			'description' => __('Sidebar principal lateral', 'dalbert'),
			'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
			));
		}
		function contact_widgets_init(){
			register_sidebar( array(
			'name' => 'Contact Widget Area',
			'id'  => 'contact-widget-area',
			'before_widget' => '<div class="center-block">',
			'after_widget' => '</div>',
			'before_title' => '<h2>',
			'after_title' => '</h2>'
			)
		);
	}
	add_action( 'widgets_init', 'contact_widgets_init');
	endif;
	// Crear Post Type Services
	if (!function_exists('create_post_type')){
		function create_post_type() {
			register_post_type( 'services',
				array(
				'labels' => array(
				'name' => 'Servicios',
				'singular_name' => 'Service',
				'add_new' => 'Add New Service',
				'add_new_item' => 'Add New Service',
				'edit_item' => 'Edit Service',
				'new_item' => 'New Service',
				'view_item' => 'View Service',
				'search_items' => 'Buscar Servicio',
				'not_found' =>  'This is empty',
				'not_found_in_trash' => 'This is empty'
				),
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true,
				'query_var' => true,
				'rewrite' => true,
				'hierarchical' => false,
				'menu_icon'   => 'dashicons-megaphone',
				'menu_position' => 26,
				'supports' => array('title','editor', 'custom-fields')
				)
			);
			register_post_type( 'catalogo',
				array(
				'labels' => array(
				'name' => 'Catalogo',
				'singular_name' => 'Catalogo',
				'add_new' => 'Add New Item',
				'add_new_item' => 'Add New Item',
				'edit_item' => 'Edit Item',
				'new_item' => 'New Item',
				'view_item' => 'View Item',
				'search_items' => 'Buscar Item',
				'not_found' =>  'This is empty',
				'not_found_in_trash' => 'This is empty'
				),
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true,
				'query_var' => true,
				'rewrite' => true,
				'hierarchical' => false,
				'menu_icon'   => 'dashicons-pressthis',
				'menu_position' => null,
				'supports' => array('title','editor', 'thumbnail', 'custom-fields' )
				)
			);
		}
	}
	if (!function_exists('taxonomias_propias') ) {
		function taxonomias_propias(){
			register_taxonomy('sector','catalogo',array(
				'hierarchical' => true,
				'label' => 'Sector Industrial',
				'query_var' => true,
				'rewrite' => true)
			);
			register_taxonomy('proveedor','catalogo',array(
				'hierarchical' => true,
				'label' => 'Proveedor',
				'query_var' => true,
				'rewrite' => true)
			);
			register_taxonomy('funcionalidad','catalogo',array(
				'hierarchical' => true,
				'label' => 'Funcionalidad',
				'query_var' => true,
				'rewrite' => true)
			);
		}
	}

	// ACTIVAR EL CUSTOMIZER
	require get_template_directory() . '/inc/customizer/customizer.php';


	// ============================================
	// ======== CATALOGUE BUILD METABOX ========
	// ============================================
	// https://github.com/WebDevStudios/CMB2/wiki/Basic-Usage
	//include the main class file
    if ( file_exists(  __DIR__ . '/inc/cmb2/init.php' ) ) {
	  require_once  __DIR__ . '/inc/cmb2/init.php';
	} elseif ( file_exists(  __DIR__ . '/inc/CMB2/init.php' ) ) {
	  require_once  __DIR__ . '/inc/CMB2/init.php';
	}

	add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );
	/**
	 * Define the metabox and field configurations.
	 */
	function cmb2_sample_metaboxes() {

	    // Start with an underscore to hide fields from custom fields list
	    $prefix = '_productotipo_';
	    $prefix2 = '_productofuncionalidad_';

	    /**
	     * Initiate the metabox
	     */
	    $cmb = new_cmb2_box( array(
	        'id'            => 'producto_tipo',
	        'title'         => __( 'Campos Especiales', 'cmb2' ),
	        'object_types'  => array( 'catalogo', ), // Post type
	        'context'       => 'normal',
	        'priority'      => 'high',
	        'show_names'    => true, // Show field names on the left
	        // 'cmb_styles' => false, // false to disable the CMB stylesheet
	        // 'closed'     => true, // Keep the metabox closed by default
	    ));

	    // Regular text field
	    $cmb->add_field( array(
	        'name'       => __( 'Tipo de Producto', 'cmb2' ),
	        'desc'       => __( 'Ingresa el tipo de producto', 'cmb2' ),
	        'id'         => $prefix . 'text',
	        'type'       => 'text',
	        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
	        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
	        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
	        // 'on_front'        => false, // Optionally designate a field to wp-admin only
	        // 'repeatable'      => true,
	    ));

	    $cmb->add_field( array(
	        'name'       => __( 'Funcionalidad', 'cmb2' ),
	        'desc'       => __( 'Ingresa la funcionalidad del producto', 'cmb2' ),
	        'id'         => $prefix2 . 'text',
	        'type'       => 'text',
	        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
	        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
	        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
	        // 'on_front'        => false, // Optionally designate a field to wp-admin only
	        // 'repeatable'      => true,
    	));

    	

	}
?>